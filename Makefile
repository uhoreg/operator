all:
	$(MAKE) -C liboperator
	$(MAKE) -C liboperator-glib
	$(MAKE) -C liboperator-qt

test:
	$(MAKE) -C liboperator test
#	$(MAKE) -C liboperator-glib test
#	$(MAKE) -C liboperator-qt test

clean:
	$(MAKE) -C liboperator clean
	$(MAKE) -C liboperator-glib clean
	$(MAKE) -C liboperator-qt clean
	rm -rf docs

examples:
#	$(MAKE) -C liboperator examples
	$(MAKE) -C liboperator-glib examples
	$(MAKE) -C liboperator-qt examples

docs:
	mkdir -p docs
	doxygen

.PHONY: all install clean test examples docs
