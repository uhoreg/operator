#include "../http.hh"

#include <iostream>
#include <string>
#include <cstring>

using namespace Operator;

GMainLoop *loop;

int main(int argc, char* const *argv)
{
  if (argc != 2 || !strcmp(argv[1], "--help"))
  {
    std::cout << "Usage:\n";
    std::cout << "  " << argv[0] << " <homeserver>\n";
    exit(0);
  }
  SoupSession *session = soup_session_new();
  loop = g_main_loop_new(nullptr, FALSE);
  Operator::GLib::HTTPAdapter adapter(session);
  Operator::Client::HTTPClientAPI client(adapter, argv[1]);
  client.versions([](std::shared_ptr<Error::Error> error, const std::vector<std::string> &versions) {
      if (error)
      {
        std::cerr << "Error: " << error->to_string() << std::endl;
      }
      else
      {
        for (std::vector<std::string>::const_iterator it = versions.begin(); it != versions.end(); ++it)
        {
          std::cout << (*it) << std::endl;
        }
      }
      g_main_loop_quit(loop);
    });
  g_main_loop_run(loop);
}
