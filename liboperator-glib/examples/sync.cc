#include "../http.hh"

#include <iostream>
#include <string>
#include <cstring>

using json = nlohmann::json;
using namespace Operator;

GMainLoop *loop;

int main(int argc, char**argv)
{
  if (argc != 4 || !strcmp(argv[1], "--help"))
  {
    std::cout << "Usage:\n";
    std::cout << "  " << argv[0] << " <homeserver> <username> <password>\n";
    exit(0);
  }

  SoupSession *session = soup_session_new();
  loop = g_main_loop_new(nullptr, FALSE);
  Operator::GLib::HTTPAdapter adapter(session);

  Operator::Client::HTTPClientAPI client(adapter, argv[1]);
  client.login(argv[2], argv[3], [&client](std::shared_ptr<Error::Error> error, const std::string &username, const std::string &homeserver) {
      if (error)
      {
        std::cerr << "Error in login: " << error->to_string() << std::endl;
        g_main_loop_quit(loop);
      }
      else
      {
        std::cout << "[Logged into " << homeserver << " as " << username << "]\n";
        client.sync("", "", false, "offline", 0, [&client](std::shared_ptr<Error::Error> error, const std::string &next_batch, json rooms, json presence) {
            if (error)
            {
              std::cerr << "Error in sync: " << error->to_string() << std::endl;
              client.logout([](std::shared_ptr<Error::Error> error) {
                  g_main_loop_quit(loop);
                });
            }
            else
            {
              std::cout << rooms << std::endl;
              client.logout([](std::shared_ptr<Error::Error> error) {
                  if (error)
                  {
                    std::cerr << "Error in logout: " << error->to_string() << std::endl;
                  }
                  g_main_loop_quit(loop);
                });
            }
          });
      }
    });
  g_main_loop_run(loop);
}
