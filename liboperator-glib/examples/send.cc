#include "../http.hh"

#include <iostream>
#include <string>
#include <cstring>

using namespace Operator;
using json = nlohmann::json;

GMainLoop *loop;

int main(int argc, char**argv)
{
  if (argc != 6 || !strcmp(argv[1], "--help"))
  {
    std::cout << "Usage:\n";
    std::cout << "  " << argv[0] << " <homeserver> <username> <password> <roomID> <message>\n";
    exit(0);
  }

  SoupSession *session = soup_session_new();
  loop = g_main_loop_new(nullptr, FALSE);
  Operator::GLib::HTTPAdapter adapter(session);

  Operator::Client::HTTPClientAPI client(adapter, argv[1]);
  client.login(argv[2], argv[3], [&client, &argv](std::shared_ptr<Error::Error> error, const std::string &username, const std::string &homeserver) {
      if (error)
      {
        std::cerr << "Error in login: " << error->to_string() << std::endl;
        g_main_loop_quit(loop);
      }
      else
      {
        std::cout << "[Logged into " << homeserver << " as " << username << "]\n";
        json msg;
        msg["body"] = argv[5];
        msg["msgtype"] = "m.text";
        client.sendMessage(argv[4], "m.room.message", "message", msg, [&client](std::shared_ptr<Error::Error> error, const std::string &eventid) {
            if (error)
            {
              std::cerr << "Error in send: " << error->to_string() << std::endl;
              client.logout([](std::shared_ptr<Error::Error> error) {
                  g_main_loop_quit(loop);
                });
            }
            else
            {
              std::cout << "sent as event ID " << eventid << std::endl;
              client.logout([](std::shared_ptr<Error::Error> error) {
                  if (error)
                  {
                    std::cerr << "Error in logout: " << error->to_string() << std::endl;
                  }
                  g_main_loop_quit(loop);
                });
            }
          });
      }
    });
  g_main_loop_run(loop);
}
