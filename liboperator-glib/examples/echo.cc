#include "../http.hh"
#include <liboperator/event.hh>

#include <iostream>
#include <string>
#include <cstring>
#include <functional>

using namespace Operator;
using json = nlohmann::json;

using namespace std::placeholders;

GMainLoop *loop;

void handleMessage(std::shared_ptr<Error::Error> error, Client::Client::Room &room, const Event::Event &event, Client::Client *pClient, const std::string *pRoom) {
  if (!error && event.getRoomID() == *pRoom)
  {
    try
    {
      const Event::Message::Message &msgevent = dynamic_cast<const Event::Message::Message &>(event);
      std::cout << "[Received message of type " << msgevent.getMessageType() << "]\n";
      try
      {
        auto &notice = dynamic_cast<const Event::Message::Notice &>(msgevent);
        return; // ignore notices
        // NOTE: this could be done more easily by just checking
        // msgevent.getMessageType(), but this is to illustrate how different
        // classes are used for different message types.
      }
      catch (std::bad_cast)
      {}

      if (msgevent.getBody() == "quit")
      {
        pClient->logout();
        return;
      }

      // create new event from old event
      json event = {
        {"room_id", msgevent.getRoomID()},
        {"type", msgevent.getType()},
        {"content", msgevent.getContent()}
      };
      pClient->sendMessage(Event::Message::Notice(event), [](std::shared_ptr<Error::Error> error, const std::string &event_id) {
          if (error)
          {
            std::cerr << "Error sending event: " << error->to_string() << std::endl;
          }
          else
          {
            std::cout << "sent event " << event_id << std::endl;
          }
        });
    }
    catch (std::bad_cast)
    {
      // ignore
    }
  }
}

int main(int argc, char**argv)
{
  if (argc != 5 || !strcmp(argv[1], "--help"))
  {
    std::cout << "Usage:\n";
    std::cout << "  " << argv[0] << " <homeserver> <username> <password> <roomid>\n";
    exit(0);
  }

  const std::string &room = argv[4];

  SoupSession *session = soup_session_new();
  loop = g_main_loop_new(nullptr, FALSE);

  Operator::GLib::HTTPAdapter adapter(session);
  Operator::Client::HTTPClient client(adapter, argv[1]);
  client.on<Event::Message::Message>(std::bind(handleMessage, _1, _2, _3, &client, &room));
  client.on<Event::Login>([&client](std::shared_ptr<Error::Error> error, Client::Client::Room &, const Event::Event &ev) {
      if (error)
      {
        std::cerr << "Error in login: " << error->to_string() << std::endl;
        g_main_loop_quit(loop);
      }
      else
      {
        try
        {
          const Event::Login &event = dynamic_cast<const Event::Login &>(ev);
          std::cout << "[Logged into " << event.getHomeServer() << " as " << event.getUserID() << "]\n";
          client.startClient();
        }
        catch (std::bad_cast)
        {
          std::cerr << "Unexpected event received for login\n";
          g_main_loop_quit(loop);
        }
      }
    });
  client.on<Event::Logout>([](std::shared_ptr<Error::Error> error, Client::Client::Room &, const Event::Event &ev) {
      g_main_loop_quit(loop);
    });
  client.login(argv[2], argv[3]);
  g_main_loop_run(loop);
}
