/** \brief glib support for HTTP-based protocols (via libsoup)
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_GLIB_HTTP_HH_
#define __OPERATOR_GLIB_HTTP_HH_

#include <glib.h>
#include <libsoup/soup.h>

#include <liboperator/http_client.hh>
#include <liboperator/error.hh>

#include <memory>

namespace Operator {
  namespace GLib {
    class HTTPAdapter : public HTTP::Adapter {
    public:
      HTTPAdapter(SoupSession *);
      void httpcall(const std::string &method, const std::string &URL, const HTTP::headers_t &, std::unique_ptr<std::istream> body, size_t timeout, const HTTP::httpcallback_t &);
    private:
      SoupSession *session;
    };

    // FIXME: move to a separate header file
    class Error : public Operator::Error::Error {
    public:
      Error(GError *_err)
        : err(_err) {}
      ~Error() {
        g_error_free(err);
      }
      std::string to_string() const { return err->message; };
      GError *getGError() const { return err; }
    private:
      GError *err;
    };
  }
}

#endif // __OPERATOR_GLIB_HTTP_HH_
