((c++-mode
  (flycheck-gcc-include-path . (".." "/usr/include/jsoncpp" "/usr/include/libsoup-2.4" "/usr/include/libxml2" "/usr/include/glib-2.0" "/usr/lib/x86_64-linux-gnu/glib-2.0/include"))
  (flycheck-gcc-language-standard . "c++11")))
