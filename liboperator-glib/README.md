Operator-GLib                                                   {#Operator-GLib}
=============

Operator-GLib provides an adapter for using Operator with the GLib library.
See the Operator::GLib namespace documentation for more information.
