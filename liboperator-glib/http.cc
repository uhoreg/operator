/** \brief glib support for HTTP-based protocols (via libsoup)
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "http.hh"

#include <iostream>
#include <sstream>
#include <functional>

using namespace Operator;
using namespace Operator::GLib;

static void handle_soup_end (SoupSession *session, SoupMessage *msg, gpointer object) {
  HTTP::httpcallback_t *callback = static_cast<HTTP::httpcallback_t *>(object);
  std::istringstream *res_body = new std::istringstream(std::string(msg->response_body->data, msg->response_body->length));
  HTTP::headers_t resp_headers;
  SoupMessageHeadersIter iter;
  soup_message_headers_iter_init(&iter, msg->response_headers);
  const char *name;
  const char *value;
  while (soup_message_headers_iter_next(&iter, &name, &value))
  {
    resp_headers.push_back({std::string(name), std::string(value)});
  }
  (*callback)(nullptr, msg->status_code, resp_headers, std::unique_ptr<std::istream>(res_body));
  delete callback;
}

HTTPAdapter::HTTPAdapter(SoupSession * s)
  : session(s) {}

void HTTPAdapter::httpcall(const std::string &method, const std::string &URL, const HTTP::headers_t &headers, std::unique_ptr<std::istream> body, size_t timeout, const HTTP::httpcallback_t &callback) {
  SoupMessage *msg = soup_message_new(method.c_str(), URL.c_str());
  if (!msg)
  {
    callback(std::unique_ptr<Operator::Error::Error>(new Operator::Error::InvalidData("Invalid URL: " + URL)), 0, {}, nullptr);
    return;
  }
  std::string b;
  if (body)
  {
    char buffer[1024];
    if (!msg->request_body)
    {
      msg->request_body = soup_message_body_new();
    }
    do
    {
      body->read(buffer, 1024);
      if (body->gcount())
      {
        soup_message_body_append(msg->request_body, SOUP_MEMORY_COPY, buffer, body->gcount());
      }
    }
    while (body && body->gcount());
  }
  if (!msg->request_headers)
  {
    msg->request_headers = soup_message_headers_new(SOUP_MESSAGE_HEADERS_REQUEST);
  }
  for (HTTP::headers_t::const_iterator it = headers.begin(); it != headers.end(); ++it)
  {
    soup_message_headers_append(msg->request_headers, it->first.c_str(), it->second.c_str());
  }
  soup_session_queue_message(session, msg, handle_soup_end, (gpointer) new HTTP::httpcallback_t(callback));
}
