/** \brief Error handling
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_ERROR_HH_
#define __OPERATOR_ERROR_HH_

#include <json.hpp>

#include <functional>
#include <map>
#include <string>

namespace Operator {
  /** \brief error handling.
   */
  namespace Error {
    /** \brief base error class.
     *
     * All errors inherit from this class.
     */
    class Error {
    protected:
      Error() {};
    public:
      virtual ~Error() {};
      virtual std::string to_string() const = 0;
    };

    /** \brief invalid data error.
     *
     * This error is used when invalid data is received from the server.
     */
    class InvalidData : public Error {
    public:
      InvalidData(const std::string &_description)
        : description(_description)
        {}
      std::string to_string() const { return description; };
    private:
      std::string description;
    };

    /** \brief base error class for Matrix protocol errors.
     * \sa [C-S r0.2.0 section 2](https://matrix.org/docs/spec/client_server/r0.2.0.html#api-standards)
     */
    class MatrixError : public Error {
    public:
      virtual const std::string &getErrorCode() const = 0;
      const std::string &getError() const { return error; }
      unsigned short getStatusCode() const { return status_code; }
      const std::map<std::string,nlohmann::json> &getData() const { return other_data; }

      static Error * newError(unsigned short status_code, const nlohmann::json &data) throw ();

      std::string to_string() const { return getErrorCode() + " (" + std::to_string(getStatusCode()) + "): " + getError(); }

      static const char M_FORBIDDEN[];
      static const char M_UNKNOWN_TOKEN[];
      static const char M_BAD_JSON[];
      static const char M_NOT_JSON[];
      static const char M_NOT_FOUND[];
      static const char M_LIMIT_EXCEEDED[];
      static const char M_USER_IN_USE[];
      static const char M_INVALID_USERNAME[];
      static const char M_ROOM_IN_USE[];
      static const char M_BAD_PAGINATION[];
      static const char M_THREEPID_IN_USE[];
      static const char M_THREEPID_NOT_FOUND[];
      static const char M_SERVER_NOT_TRUSTED[];
      static const char M_WEAK_PASSWORD[];
      static const char M_UNKNOWN[];
      static const char M_EXCLUSIVE[];
      static const char M_MISSING_PARAM[];

      typedef std::function<MatrixError *(unsigned short, const std::string &, const std::string &, std::map<std::string,nlohmann::json> &)> error_builder_t;
      static void registerErrorClass(const std::string &error_code, error_builder_t);
      template<typename T> static MatrixError *_new_helper(unsigned short _status_code, const std::string &, const std::string &_error, std::map<std::string,nlohmann::json> &_other_data) {
        return new T(_status_code, _error, _other_data);
      }
    private:
      unsigned short status_code;
      std::string error;
      std::map<std::string,nlohmann::json> other_data;
    protected:
      MatrixError(unsigned short _status_code, const std::string &_error, std::map<std::string,nlohmann::json> &_other_data)
        : status_code(_status_code),
          error(_error),
          other_data(std::move(_other_data))
        {}
    };

    template<const char *c> class _MatrixErr : public MatrixError {
    public:
      _MatrixErr(unsigned short _status_code, std::string _error, std::map<std::string,nlohmann::json> &_other_data)
        : MatrixError(_status_code, _error, _other_data) {}

      const std::string &getErrorCode() const { return ERROR_CODE; }
      static const std::string ERROR_CODE;
    };

    /** \brief Forbidden access, e.g. joining a room without permission, failed login.
     * \class Operator::Error::MatrixForbidden
     * \extends MatrixError
     */
    using MatrixForbidden = _MatrixErr<MatrixError::M_FORBIDDEN>;
    /** \brief The access token specified was not recognised.
     * \class Operator::Error::MatrixUnknownToken
     * \extends MatrixError
     */
    using MatrixUnknownToken = _MatrixErr<MatrixError::M_UNKNOWN_TOKEN>;
    /** \brief Request contained valid JSON, but it was malformed in some way, e.g. missing required keys, invalid values for keys.
     * \class Operator::Error::MatrixBadJson
     * \extends MatrixError
     */
    using MatrixBadJson = _MatrixErr<MatrixError::M_BAD_JSON>;
    /** \brief Request did not contain valid JSON.
     * \class Operator::Error::MatrixNotJson
     * \extends MatrixError
     */
    using MatrixNotJson = _MatrixErr<MatrixError::M_NOT_JSON>;
    /** \brief No resource was found for this request.
     * \class Operator::Error::MatrixNotFound
     * \extends MatrixError
     */
    using MatrixNotFound = _MatrixErr<MatrixError::M_NOT_FOUND>;
    /** \brief Too many requests have been sent in a short period of time. Wait a while then try again.
     * \class Operator::Error::MatrixLimitExceeded
     * \extends MatrixError
     */
    using MatrixLimitExceeded = _MatrixErr<MatrixError::M_LIMIT_EXCEEDED>;
    /** \brief Encountered when trying to register a user ID which has been taken.
     * \class Operator::Error::MatrixUserInUse
     * \extends MatrixError
     */
    using MatrixUserInUse = _MatrixErr<MatrixError::M_USER_IN_USE>;
    /** \brief Encountered when trying to register a user ID which is not valid.
     * \class Operator::Error::MatrixInvalidUsername
     * \extends MatrixError
     */
    using MatrixInvalidUsername = _MatrixErr<MatrixError::M_INVALID_USERNAME>;
    /** \brief Encountered when trying to create a room which has been taken.
     * \class Operator::Error::MatrixRoomInUse
     * \extends MatrixError
     */
    using MatrixRoomInUse = _MatrixErr<MatrixError::M_ROOM_IN_USE>;
    /** \brief Encountered when specifying bad pagination query parameters.
     * \class Operator::Error::MatrixBadPagination
     * \extends MatrixError
     */
    using MatrixBadPagination = _MatrixErr<MatrixError::M_BAD_PAGINATION>;
    /** \brief Sent when a threepid given to an API cannot be used because the same threepid is already in use.
     * \class Operator::Error::MatrixThreepidInUse
     * \extends MatrixError
     */
    using MatrixThreepidInUse = _MatrixErr<MatrixError::M_THREEPID_IN_USE>;
    /** \brief Sent when a threepid given to an API cannot be used because no record matching the threepid was found.
     * \class Operator::Error::MatrixThreepidNotFound
     * \extends MatrixError
     */
    using MatrixThreepidNotFound = _MatrixErr<MatrixError::M_THREEPID_NOT_FOUND>;
    /** \brief The client's request used a third party server, eg. ID server, that this server does not trust.
     * \class Operator::Error::MatrixServerNotTrusted
     * \extends MatrixError
     */
    using MatrixServerNotTrusted = _MatrixErr<MatrixError::M_SERVER_NOT_TRUSTED>;
    /** \brief The password is too weak.
     * \class Operator::Error::MatrixWeakPassword
     * \extends MatrixError
     */
    using MatrixWeakPassword = _MatrixErr<MatrixError::M_WEAK_PASSWORD>;
    /** \brief Unknown.
     * \class Operator::Error::MatrixUnknown
     * \extends MatrixError
     */
    using MatrixUnknown = _MatrixErr<MatrixError::M_UNKNOWN>;
    /** \brief The desired user ID is in the exclusive namespace claimed by an application service.
     * \class Operator::Error::MatrixExclusive
     * \extends MatrixError
     */
    using MatrixExclusive = _MatrixErr<MatrixError::M_EXCLUSIVE>;
    /** \brief A parameter is missing.
     * \class Operator::Error::MatrixMissingParam
     * \extends MatrixError
     */
    using MatrixMissingParam = _MatrixErr<MatrixError::M_MISSING_PARAM>;

    /** \brief Unknown (possibly custom) Matrix error
     */
    class OtherMatrixError : public MatrixError {
    public:
      OtherMatrixError(unsigned short _status_code, const std::string &_error_code, const std::string &_error, std::map<std::string,nlohmann::json> &_other_data)
        : MatrixError(_status_code, _error, _other_data),
          error_code(_error_code)
        {}
      const std::string &getErrorCode() const { return error_code; }
    private:
      const std::string error_code;
    };
  }
}

#endif // __OPERATOR_ERROR_HH_
