/** \brief Common HTTP definitions
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_HTTP_HH_
#define __OPERATOR_HTTP_HH_

#include "error.hh"

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <memory>
#include <functional>

namespace Operator {
  /** \brief Common HTTP definitions.
   */
  namespace HTTP {
    /** \brief HTTP header.
     */
    typedef std::pair<std::string, std::string> header_t;
    /** \brief list of HTTP headers.
     */
    typedef std::vector<header_t> headers_t;
    /** \brief HTTP callback type.
     */
    typedef std::function<void (std::unique_ptr<Error::Error>, short code, const headers_t &, std::unique_ptr<std::istream> body)> httpcallback_t;

    /** \brief HTTP adapter interface
     */
    class Adapter {
    public:
      virtual void httpcall(const std::string &method, const std::string &URL, const headers_t &, std::unique_ptr<std::istream> body, size_t timeout, const httpcallback_t &) = 0;
    };

    struct url_encode;

    std::ostream & operator<<(std::ostream &os, const url_encode &ue);

    struct url_encode {
      const std::string &orig;
      url_encode(const std::string &o) : orig(o) {}
      operator std::string() const {
        std::ostringstream out;
        out << (*this);
        return out.str();
      }
    };
  }
}
#endif // __OPERATOR_HTTP_HH_
