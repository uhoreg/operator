/** \brief data types for Matrix
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_STRUCTS_HH_
#define __OPERATOR_STRUCTS_HH_

#include "error.hh"

#include <string>
#include <json.hpp>

namespace Operator {
  struct Filter {
    // TODO:
  };

  struct RoomFilter {
    // TODO:
  };

  struct RoomEventFilter {
    // TODO:
  };

  /** \brief general type for callbacks
   */
  template<typename... T> using callback_t = typename std::function<void (std::shared_ptr<Error::Error> err, T...)>;
  template<typename... T> void noop_callback(std::shared_ptr<Error::Error> err, T...) {}
}

#endif // __OPERATOR_STRUCTS_HH_
