/** \brief client functionality using the HTTP-based API
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "http_client.hh"

#include <json.hpp>

#include <sstream>
#include <functional>

using namespace Operator;
using namespace Operator::Client;
using namespace Operator::HTTP;

using namespace std::placeholders;

using json = nlohmann::json;

/*******************************************************************************
 * HTTPClientAPI
 ******************************************************************************/

HTTPClientAPI::HTTPClientAPI(Adapter &_adapter, const std::string &_server)
  : adapter(_adapter),
    baseurl(_server + ((_server[_server.size()-1] == '/') ? "_matrix/" : "/_matrix/"))
{}

namespace {
  void handleVersions(std::unique_ptr<Error::Error> err, short code, const headers_t &headers, std::unique_ptr<std::istream> body, callback_t<const std::vector<std::string> &> callback) throw () {
    if (err)
    {
      std::vector<std::string> versions;
      callback(std::move(err), versions);
      return;
    }
    try
    {
      switch(code)
      {
      case 200:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        if (!root.is_object())
        {
          throw new Error::InvalidData("Body not an object");
        }
        auto versionsValue = root.find("versions");
        if (versionsValue == root.end() || !versionsValue->is_array())
        {
          throw new Error::InvalidData("Versions not an array");
        }
        std::vector<std::string> versions;
        for (json::iterator it = versionsValue->begin(); it != versionsValue->end(); ++it)
        {
          if (!(it->is_string()))
          {
            throw new Error::InvalidData("Version item not a string");
          }
          versions.push_back(*it);
        }
        callback(nullptr, versions);
        break;
      }
      default:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        throw Error::MatrixError::newError(code, root);
      }
      }
    }
    catch (Error::Error *e)
    {
      std::vector<std::string> versions;
      callback(std::shared_ptr<Error::Error>(e), versions);
    }
  }
}

void HTTPClientAPI::versions(const callback_t<const std::vector<std::string> &> &callback) {
  headers_t headers;
  adapter.httpcall("GET", baseurl + "client/versions", headers, nullptr, 0, std::bind(handleVersions, _1, _2, _3, _4, callback));
}

void HTTPClientAPI::login(const std::string &username, const std::string &password, const callback_t<const std::string &,const std::string &> &callback) {
  json params({
      {"type", "m.login.password"},
      {"user", username},
      {"password", password}
    });
  std::stringstream *body = new std::stringstream;
  (*body) << params;
  headers_t headers({{"Content-Type", "application/json"},
      {"Content-Length", std::to_string(body->str().size())}});
  adapter.httpcall("POST", baseurl + "client/r0/login", headers, std::unique_ptr<std::istream>(body), 0, std::bind(&HTTPClientAPI::handleLogin, this, _1, _2, _3, _4, callback));
}

void HTTPClientAPI::loginWith3PID(const std::string &medium, const std::string &address, const std::string &password, const callback_t<const std::string &,const std::string &> &callback) {
  json params({
      {"type", "m.login.password"},
      {"medium", medium},
      {"address", address},
      {"password", password}
    });
  std::stringstream *body = new std::stringstream;
  (*body) << params;
  headers_t headers({{"Content-Type", "application/json"},
      {"Content-Length", std::to_string(body->str().size())}});
  adapter.httpcall("POST", baseurl + "client/r0/login", headers, std::unique_ptr<std::istream>(body), 0, std::bind(&HTTPClientAPI::handleLogin, this, _1, _2, _3, _4, callback));
}

void HTTPClientAPI::handleLogin(std::unique_ptr<Error::Error> err, short code, const headers_t &headers, std::unique_ptr<std::istream> body, callback_t<const std::string &,const std::string&> callback) throw () {
  if (err)
  {
    callback(std::move(err), "", "");
    return;
  }
  try
  {
    switch(code)
    {
    case 200:
    {
      json root;
      try
      {
        (*body) >> root;
      }
      catch (std::invalid_argument)
      {
        throw new Error::InvalidData("Body is not JSON");
      }
      if (!root.is_object())
      {
        throw new Error::InvalidData("Body not an object");
      }
      auto tokenValue = root.find("access_token");
      auto userIdValue = root.find("user_id");
      auto homeServerValue = root.find("home_server");
      if (tokenValue == root.end() || !tokenValue->is_string()
          || userIdValue == root.end() || !userIdValue->is_string()
          || homeServerValue == root.end() || !homeServerValue->is_string())
      {
        throw new Error::InvalidData("Missing or malformed required value");
      }
      token = HTTP::url_encode(tokenValue->get_ref<const std::string &>());
      callback(nullptr, userIdValue->get_ref<const std::string &>(), homeServerValue->get_ref<const std::string &>());
      break;
    }
    default:
    {
      json root;
      try
      {
        (*body) >> root;
      }
      catch (std::invalid_argument)
      {
        throw new Error::InvalidData("Body is not JSON");
      }
      throw Error::MatrixError::newError(code, root);
    }
    }
  }
  catch (Error::Error *e)
  {
    callback(std::shared_ptr<Error::Error>(e), "", "");
  }
}

void HTTPClientAPI::loginWithToken(const std::string &_token) {
  token = HTTP::url_encode(_token);
}

void HTTPClientAPI::logout(const callback_t<> &callback) {
  if (!isLoggedIn())
  {
    callback(nullptr);
    return;
  }
  headers_t headers;
  adapter.httpcall("POST", baseurl + "client/r0/logout?access_token=" + getToken(), headers, nullptr, 0, std::bind(&HTTPClientAPI::handleLogout, this, _1, _2, _3, _4, callback));
}

void HTTPClientAPI::handleLogout(std::unique_ptr<Error::Error> err, short code, const headers_t &headers, std::unique_ptr<std::istream> body, callback_t<> callback) throw () {
  if (err)
  {
    callback(std::move(err));
    return;
  }
  try
  {
    switch(code)
    {
    case 200:
    {
      token.clear();
      callback(nullptr);
      break;
    }
    default:
    {
      json root;
      try
      {
        (*body) >> root;
      }
      catch (std::invalid_argument)
      {
        throw new Error::InvalidData("Body is not JSON");
      }
      throw Error::MatrixError::newError(code, root);
    }
    }
  }
  catch (Error::Error *e)
  {
    callback(std::shared_ptr<Error::Error>(e));
  }
}

namespace {
  void handleSync(std::unique_ptr<Error::Error> err, short code, const headers_t &headers, std::unique_ptr<std::istream> body, callback_t<const std::string &, json, json> callback) throw () {
    if (err)
    {
      callback(std::move(err), "", json(), json());
      return;
    }
    try
    {
      switch(code)
      {
      case 200:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        if (!root.is_object())
        {
          throw new Error::InvalidData("Body not an object");
        }
        auto next_batch = root.find("next_batch");
        auto rooms = root.find("rooms");
        auto presence = root.find("presence");
        if (next_batch == root.end() || !next_batch->is_string()
            || rooms == root.end() || !rooms->is_object()
            || presence == root.end() || !presence->is_object())
        {
          throw new Error::InvalidData("Missing or malformed required value");
        }
        callback(nullptr, next_batch->get_ref<const std::string &>(), std::move(*rooms), std::move(*presence));
        break;
      }
      default:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        throw Error::MatrixError::newError(code, root);
      }
      }
    }
    catch (Error::Error *e)
    {
      callback(std::shared_ptr<Error::Error>(e), "", json(), json());
    }
  }
}

void HTTPClientAPI::sync(const std::string &since, const std::string &filter, bool full_state, std::string set_presence, size_t timeout, const callback_t<const std::string &, json, json> &callback) {
  std::ostringstream urlstream;
  urlstream << baseurl << "client/r0/sync?access_token=" << getToken();
  if (!since.empty())
  {
    urlstream << "&since=" << HTTP::url_encode(since);
  }
  if (!filter.empty())
  {
    urlstream << "&filter=" << HTTP::url_encode(filter);
  }
  if (full_state)
  {
    urlstream << "&full_state=true";
  }
  if (!set_presence.empty())
  {
    urlstream << "&set_presence=" << HTTP::url_encode(set_presence);
  }
  if (timeout)
  {
    urlstream << "&timeout=" << timeout;
  }
  headers_t headers;
  adapter.httpcall("GET", urlstream.str(), headers, nullptr, (timeout/1000) + 1, std::bind(handleSync, _1, _2, _3, _4, callback));
}

namespace {
  void handleRoomMessages(std::unique_ptr<Error::Error> err, short code, const headers_t &headers, std::unique_ptr<std::istream> body, callback_t<const std::string &, const std::string &, const std::vector<json> &> callback) throw () {
    if (err)
    {
      callback(std::move(err), "", "", {});
      return;
    }
    try
    {
      switch(code)
      {
      case 200:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        if (!root.is_object())
        {
          throw new Error::InvalidData("Body not an object");
        }
        auto start = root.find("start");
        auto end = root.find("end");
        auto chunk = root.find("chunk");
        if (chunk == root.end() || !chunk->is_array())
        {
          throw new Error::InvalidData("Chunk not an array");
        }
        callback(nullptr, start->is_string() ? start->get_ref<const std::string &>() : "", end->is_string() ? end->get_ref<const std::string &>() : "", std::vector<json>(chunk->begin(), chunk->end()));
        break;
      }
      default:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        throw Error::MatrixError::newError(code, root);
      }
      }
    }
    catch (Error::Error *e)
    {
      callback(std::shared_ptr<Error::Error>(e), "", "", {});
    }
  }
}

void HTTPClientAPI::roomMessages(const std::string &roomId, const std::string &from, const std::string &to, const std::string &dir, int limit, const callback_t<const std::string &, const std::string &, const std::vector<nlohmann::json> &> &callback) {
  std::ostringstream urlstream;
  urlstream << baseurl << "client/r0/rooms/" << HTTP::url_encode(roomId) << "/messages?access_token=" << getToken()
            << "&from=" << HTTP::url_encode(from) << "&dir=" << HTTP::url_encode(dir);
  if (!to.empty())
  {
    urlstream << "&to=" << HTTP::url_encode(to);
  }
  if (limit > 0)
  {
    urlstream << "&limit=" << limit;
  }
  headers_t headers;
  adapter.httpcall("GET", urlstream.str(), headers, nullptr, 0, std::bind(handleRoomMessages, _1, _2, _3, _4, callback));
}

namespace {
  void handleSendMessage(std::unique_ptr<Error::Error> err, short code, const headers_t &headers, std::unique_ptr<std::istream> body, callback_t<const std::string &> callback) throw () {
    if (err)
    {
      callback(std::move(err), "");
      return;
    }
    try
    {
      switch(code)
      {
      case 200:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        if (!root.is_object())
        {
          throw new Error::InvalidData("Body not an object");
        }
        auto event_id = root.find("event_id");
        if (event_id == root.end() || !event_id->is_string())
        {
          throw new Error::InvalidData("Invalid event ID");
        }
        callback(nullptr, event_id->get_ref<const std::string &>());
        break;
      }
      default:
      {
        json root;
        try
        {
          (*body) >> root;
        }
        catch (std::invalid_argument)
        {
          throw new Error::InvalidData("Body is not JSON");
        }
        throw Error::MatrixError::newError(code, root);
      }
      }
    }
    catch (Error::Error *e)
    {
      callback(std::shared_ptr<Error::Error>(e), "");
    }
  }
}

void HTTPClientAPI::sendMessage(const std::string &roomId, const std::string &eventType, const std::string &txnId, const nlohmann::json &content, const callback_t<const std::string &> &callback) {
  std::ostringstream urlstream;
  urlstream << baseurl << "client/r0/rooms/" << HTTP::url_encode(roomId) << "/send/" << HTTP::url_encode(eventType) << "/" << HTTP::url_encode(txnId) << "?access_token=" << getToken();
  std::stringstream *body = new std::stringstream;
  (*body) << content;
  headers_t headers({{"Content-Type", "application/json"},
      {"Content-Length", std::to_string(body->str().size())}});
  adapter.httpcall("PUT", urlstream.str(), headers, std::unique_ptr<std::istream>(body), 0, std::bind(handleSendMessage, _1, _2, _3, _4, callback));
}

/*******************************************************************************
 * HTTPClient
 ******************************************************************************/

HTTPClient::HTTPClient(HTTP::Adapter &adapter, const std::string &server)
  : client(adapter, server),
    handleSync_ptr(std::bind(std::mem_fn(&HTTPClient::handleSync), this, _1, _2, _3, _4)),
    trigger_ptr(std::bind(std::mem_fn(&HTTPClient::trigger), this, _1)),
    triggerError_ptr(std::bind(std::mem_fn(&HTTPClient::triggerError), this, _1, _2, _3))
{}

namespace {
  void clientLoginCallback(std::shared_ptr<Error::Error> error, const std::string &userid, const std::string &server, std::function<void (const Event::Event &)> &trigger, std::function<void (const std::string &, std::shared_ptr<Error::Error>, const std::string &)> triggerError, const callback_t<const std::string &,const std::string &> &callback)
  {
    if (error)
    {
      triggerError(Event::Login::TYPE, error, "");
    }
    else
    {
      trigger(Event::Login(userid, server));
    }
    callback(std::move(error), userid, server);
  }
}

void HTTPClient::login(const std::string &username, const std::string &password, const callback_t<const std::string &,const std::string &> &callback)
{
  client.login(username, password, std::bind(clientLoginCallback, _1, _2, _3, trigger_ptr, triggerError_ptr, callback));
}

void HTTPClient::loginWith3PID(const std::string &medium, const std::string &address, const std::string &password, const callback_t<const std::string &,const std::string &> &callback)
{
  client.loginWith3PID(medium, address, password, std::bind(clientLoginCallback, _1, _2, _3, trigger_ptr, triggerError_ptr, callback));
}

namespace {
  void clientLogoutCallback(std::shared_ptr<Error::Error> error, std::function<void (const Event::Event &)> &trigger, std::function<void (const std::string &, std::shared_ptr<Error::Error>, const std::string &)> triggerError, const callback_t<> &callback)
  {
    if (error)
    {
      triggerError(Event::Logout::TYPE, error, "");
    }
    else
    {
      trigger(Event::Logout());
    }
    callback(std::move(error));
  }
}

void HTTPClient::logout(const callback_t<> &callback)
{
  stopClient();
  client.logout(std::bind(clientLogoutCallback, _1, trigger_ptr, triggerError_ptr, callback));
}

void HTTPClient::sendMessage(const Event::MessageEvent &message, const callback_t<const std::string &> &callback)
{
  // FIXME: add time to transaction ID?
  client.sendMessage(message.getRoomID(), message.getType(), std::to_string(++txn_counter), message.getContent(), callback);
}

void HTTPClient::startClient() {
  if (!sync_active)
  {
    sync_active = true;
    if (!sync_pending)
    {
      sync_pending = true;
      client.sync(next_batch, "", false, "", 0, handleSync_ptr);
    }
  }
}

void HTTPClient::stopClient() {
  sync_active = false;
}

void HTTPClient::handleSync(std::shared_ptr<Error::Error>err, const std::string &next, json ev_rooms, json presence) throw () {
  if (err)
  {
    if (dynamic_cast<Error::MatrixUnknownToken *>(err.get()))
    { // access token has been invalidated, so we're logged out
      trigger(Event::Logout());
      client.loginWithToken("");
      sync_active = false;
      sync_pending = false;
      return;
    }
    // FIXME: wait and retry w/ exp. backoff?  For now, just stop
    // FIXME: call a callback
    sync_active = false;
    sync_pending = false;
  }
  if (sync_active)
  {
    next_batch = next;

    auto ev_rooms_it = ev_rooms.find("join");
    if (ev_rooms_it != ev_rooms.end())
    {
      for (auto room_it = ev_rooms_it->begin(); room_it != ev_rooms_it->end(); ++room_it)
      {
        std::string room_id = room_it.key();

        // update local copy of room
        auto client_room_it = rooms.find(room_id);
        if (client_room_it == rooms.end())
        {
          std::tie(client_room_it, std::ignore) = rooms.emplace("room_id", Room(*this, room_id, Event::RoomMembership::JOIN));
          trigger(Event::RoomMembership(Event::RoomMembership::JOIN, room_id));
        }
        else if (client_room_it->second.getMembership() != Event::RoomMembership::JOIN)
        {
          _setRoomMembership(client_room_it->second, Event::RoomMembership::JOIN);
          trigger(Event::RoomMembership(Event::RoomMembership::JOIN, room_id));
        }

        // TODO: unread_notifications, state, account_data, ephemeral

        // timeline events
        auto timeline_it = room_it->find("timeline");
        if (timeline_it != room_it->end())
        {
          // FIXME: send synthetic event if limited
          auto limited_it = timeline_it->find("limited");
          if (limited_it != timeline_it->end() && limited_it->is_boolean() && limited_it->get<bool>())
          { // FIXME: add prev_batch
            trigger(Event::Limited(room_id));
          }
          auto events_it = timeline_it->find("events");
          if (events_it != timeline_it->end())
          {
            for (auto event_it = events_it->begin(); event_it != events_it->end(); ++event_it)
            {
              std::unique_ptr<Event::Event> ev = Event::MessageEvent::newEvent(std::move(*event_it), room_id);
              trigger(*ev);
            }
          }
        }
      }
    }
    // TODO: handle leave, invite rooms

    // TODO: handle presence events

    if (sync_active) // check again in case one of the handlers stopped sync
    {
      client.sync(next_batch, "", false, "", 0, handleSync_ptr);
    }
    else
    {
      sync_pending = false;
    }
  }
  else
  {
    sync_pending = false;
  }
}
