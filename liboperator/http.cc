/** \brief Common HTTP definitions
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "http.hh"

#include <iomanip>

using namespace Operator;
using namespace Operator::HTTP;

std::ostream & Operator::HTTP::operator<<(std::ostream &os, const url_encode &ue) {
  for (std::string::const_iterator it = ue.orig.begin(); it != ue.orig.end(); ++it)
  {
    if (std::isalnum(*it) || *it == '-' || *it == '_' || *it == '.' || *it == '~')
    {
      os << *it;
    }
    else
    {
      os << '%' << std::setfill('0') << std::setw(2) << std::hex << ((int) *it);
    }
  }
  return os;
}
