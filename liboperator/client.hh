/** \brief client functionality
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_CLIENT_HH_
#define __OPERATOR_CLIENT_HH_

#include "structs.hh"
#include "event.hh"

#include <memory>
#include <string>
#include <map>
#include <list>
#include <set>

namespace Operator {
  namespace Client {
    /** \brief Matrix client interface for high-level API.
     * \interface Client
     *
     * This interface is implemented by the high-level API classes.  All
     * high-level API classes implement this, no matter which low-level API is
     * used (e.g. HTTP)
     */
    class Client {
    public:
      Client();

      class Room {
      public:
        Room(Client &c, const std::string &_id, Event::RoomMembership::membership_t _membership)
          : client(c), id(_id), membership(_membership) {}

        const std::string &getID() const { return id; }
        Event::RoomMembership::membership_t getMembership() const { return membership; }

        void on(const std::string &type, const callback_t<Room &, const Event::Event &> &);

      protected:
        Client &client;
        std::string id;
        Event::RoomMembership::membership_t membership;
        std::map<std::string, nlohmann::json> state;
        std::map<std::string, std::list<callback_t<Room &, const Event::Event &> > > listeners;

        friend class ::Operator::Client::Client;
      };

      virtual ~Client() {}

      /** \brief log in using username and password
       * \param[in] username The username to log in as
       * \param[in] password The password to use
       * \param[out] callback The callback will receive the user's Matrix ID
       * and the server name
       * \sa (C-S r0.2.0 section 3.2.1)
       */
      virtual void login(const std::string &username, const std::string &password, const callback_t<const std::string &,const std::string &> & = noop_callback<const std::string &,const std::string &>) = 0;

      /** \brief log in using third party ID and pasword.
       * \sa (C-S r0.2.0 section 3.2.1)
       */
      virtual void loginWith3PID(const std::string &medium, const std::string &address, const std::string &password, const callback_t<const std::string &,const std::string &> & = noop_callback<const std::string &,const std::string &>) = 0;

      /** \brief log out.
       * \sa (C-S r0.2.0 section 3.2.2)
       */
      virtual void logout(const callback_t<> & = noop_callback<>) = 0;

      /** \brief determine if the client is currently logged in.
       */
      virtual bool isLoggedIn() const = 0;

      /** \brief start listening for new events.
       */
      virtual void startClient() = 0;
      /** \brief stop listening for new events.
       */
      virtual void stopClient() = 0;

      /** \brief send a message event.
       *
       * \param[in] message The message event to send.
       * \param[out] callback The callback will receive the resulting event ID.
       */
      virtual void sendMessage(const Event::MessageEvent &message, const callback_t<const std::string &> &callback = noop_callback<const std::string &>) = 0;

      /** \brief return the room with the given ID.
       *
       * \param[in] room_id The room ID.
       */
      Room &getRoom(const std::string &room_id) {
        auto room_it = rooms.find(room_id);
        if (room_it != rooms.end())
        {
          return room_it->second;
        }
        else
        {
          rooms.emplace(room_id, Room(*this, room_id, Event::RoomMembership::NONE));
        }
      }
      const Room &getRoom(const std::string &room_id) const throw (std::out_of_range) {
        return rooms.at(room_id);
      }

      Room &operator[](const std::string &room_id) {
        return getRoom(room_id);
      }
      const Room &operator[](const std::string &room_id) const throw (std::out_of_range) {
        return getRoom(room_id);
      }

      /** \brief register an event handler.
       *
       * Register an event handler to be called when an event of the given type
       * is received.
       *
       * \param[in] type The event type, or "*" for all events.
       * \param[out] callback The callback will receive the room that the event
       * belongs to and the event.
       */
      void on(const std::string &type, const callback_t<Room &, const Event::Event &> &callback);
      /** \brief register an event handler.
       *
       * Register an event handler to be called when an event of the given type
       * is received.
       *
       * \note This is just syntactic sugar for on(T::TYPE, callback).  That
       * is, when determining which callbacks to call, it only compares the
       * event's type field with T::TYPE, and does not check the actual class
       * of the event generated.  One consequence of this is that event passed
       * to the callback may not necessarily be an object of class T.
       *
       * \tparam T the event class (use Operator::Event::Event for all events)
       * \param[out] callback The callback will receive the room that the event
       * belongs to and the event.
       */
      template <class T> inline void on(const callback_t<Room &, const Event::Event &> &callback) { on(T::TYPE, callback); }
    protected:
      /** \brief call event handlers for the given event.
       */
      void trigger(const Event::Event &event);
      void triggerError(const std::string &event_type, std::shared_ptr<Error::Error> error, const std::string &room_id = "");
      std::map<std::string, Room> rooms;

      void _setRoomMembership(Room &room, Event::RoomMembership::membership_t membership) {
        room.membership = membership;
      }
    private:
      std::map<std::string, std::list<callback_t<Room &, const Event::Event &> > > listeners;
    };

    /** \brief send a message event.
     *
     * \param[in] client The client to send with.
     * \param[in] message The message event to send.
     */
    inline Client &operator<<(Client &client, const Event::MessageEvent &message) {
      client.sendMessage(message, noop_callback<const std::string &>);
      return client;
    }
  }
}

#endif // __OPERATOR_CLIENT_HH_
