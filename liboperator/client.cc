/** \brief client functionality
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client.hh"

#include <json.hpp>

using namespace Operator;
using namespace Operator::Client;

using json = nlohmann::json;

Operator::Client::Client::Client()
{
  rooms.emplace("", Room(*this, "", Event::RoomMembership::NONE));
}

void Operator::Client::Client::on(const std::string &type, const callback_t<Client::Room &, const Event::Event &> &callback)
{
  listeners[type].push_back(callback);
}

void Operator::Client::Client::trigger(const Event::Event &event)
{
  std::string type = event.getType();
  auto room_it = rooms.find(event.getRoomID());
  if (room_it == rooms.end())
  {
    room_it = rooms.find("");
  }
  Room &room = room_it->second;

  auto listeners_it = listeners.find("*");
  if (listeners_it != listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(nullptr, room, event);
    }
  }

  listeners_it = listeners.find(type);
  if (listeners_it != listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(nullptr, room, event);
    }
  }

  listeners_it = room.listeners.find("*");
  if (listeners_it != room.listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(nullptr, room, event);
    }
  }

  listeners_it = room.listeners.find(type);
  if (listeners_it != room.listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(nullptr, room, event);
    }
  }
}

void Operator::Client::Client::triggerError(const std::string &type, std::shared_ptr<Error::Error> error, const std::string &room_id)
{
  auto room_it = rooms.find(room_id);
  if (room_it == rooms.end())
  {
    room_it = rooms.find("");
  }
  Room &room = room_it->second;

  Event::Event event({
      {"type", "ca.uhoreg.operator.__internal__.error"},
      {"content", {"body", error->to_string()}}
    }, "");

  auto listeners_it = listeners.find("*");
  if (listeners_it != listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(error, room, event);
    }
  }

  listeners_it = listeners.find(type);
  if (listeners_it != listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(error, room, event);
    }
  }

  listeners_it = room.listeners.find("*");
  if (listeners_it != room.listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(nullptr, room, event);
    }
  }

  listeners_it = room.listeners.find(type);
  if (listeners_it != room.listeners.end())
  {
    for (auto it = listeners_it->second.begin(); it != listeners_it->second.end(); ++it)
    {
      (*it)(nullptr, room, event);
    }
  }
}


void Operator::Client::Client::Room::on(const std::string &type, const callback_t<Client::Room &, const Event::Event &> &callback)
{
  listeners[type].push_back(callback);
}
