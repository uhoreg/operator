CXXFLAGS=-Wall -std=c++11 -g

OBJS = $(patsubst %.cc,%.o,$(wildcard *.cc))
TEST_OBJS = $(patsubst %.cc,%.o,$(wildcard tests/*.cc))
TESTS = $(patsubst %.cc,%.test,$(wildcard tests/*.cc))

all: liboperator.a

test: $(TESTS)
	@set -e; for test in $(TESTS); do \
	  echo "* Running $$test:" ; \
	  $$test ; \
	done

tests/%.test: tests/%.o liboperator.a
	$(CXX) -o $@ $^

liboperator.a: $(patsubst %,liboperator.a(%),$(OBJS))
	ranlib $@

clean:
	find . -name \*.o -delete
	rm -f liboperator.a
	rm -f $(TESTS)

%.d: %.cc
	@set -e; rm -f $@; \
	 $(CC) -MM $(CPPFLAGS) -std=c++11 $< > $@.$$$$; \
	 sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	 rm -f $@.$$$$

include $(OBJS:.o=.d)
include $(TEST_OBJS:.o=.d)

.PHONY: all install clean test
