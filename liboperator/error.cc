/** \brief Error handling
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "error.hh"

using namespace Operator;
using namespace Operator::Error;

namespace {
#define errorPair(s, c) {MatrixError::s, MatrixError::_new_helper<Matrix##c>}
  std::map<std::string,MatrixError::error_builder_t> errorBuilderMap = {
    errorPair(M_FORBIDDEN, Forbidden),
    errorPair(M_UNKNOWN_TOKEN, UnknownToken),
    errorPair(M_BAD_JSON, BadJson),
    errorPair(M_NOT_JSON, NotJson),
    errorPair(M_LIMIT_EXCEEDED, LimitExceeded),
    errorPair(M_USER_IN_USE, UserInUse),
    errorPair(M_INVALID_USERNAME, InvalidUsername),
    errorPair(M_ROOM_IN_USE, RoomInUse),
    errorPair(M_BAD_PAGINATION, BadPagination),
    errorPair(M_THREEPID_IN_USE, ThreepidInUse),
    errorPair(M_THREEPID_NOT_FOUND, ThreepidNotFound),
    errorPair(M_SERVER_NOT_TRUSTED, ServerNotTrusted),
    errorPair(M_WEAK_PASSWORD, WeakPassword),
    errorPair(M_UNKNOWN, Unknown),
    errorPair(M_EXCLUSIVE, Exclusive),
    errorPair(M_MISSING_PARAM, MissingParam)
  };
}

void MatrixError::registerErrorClass(const std::string &error_code, error_builder_t builder)
{
  errorBuilderMap[error_code] = builder;
}

Error::Error *MatrixError::newError(unsigned short status_code, const nlohmann::json &data) throw ()
{
  if (!data.is_object())
  {
    return new InvalidData("Error data is not an object.");
  }
  auto errcode_it = data.find("errcode"),
    error_it = data.find("error");
  if (errcode_it == data.end() || !errcode_it->is_string()
      || error_it == data.end() || !error_it->is_string())
  {
    return new InvalidData("Error data is missing error code and/or error string.");
  }
  auto builder_it = errorBuilderMap.find(*errcode_it);
  std::map<std::string,nlohmann::json> other_data;
  for (auto it = data.begin(); it != data.end(); ++it)
  {
    if (it.key() != "errcode" && it.key() != "error")
    {
      other_data[it.key()] = it.value();
    }
  }
  if (builder_it != errorBuilderMap.end())
  {
    return builder_it->second(status_code, *errcode_it, *error_it, other_data);
  }
  else
  {
    return new OtherMatrixError(status_code, *errcode_it, *error_it, other_data);
  }
}

#define makeError(s) const char MatrixError::s[] = #s; template <> const std::string _MatrixErr<MatrixError::s>::ERROR_CODE(MatrixError::s)
makeError(M_FORBIDDEN);
makeError(M_UNKNOWN_TOKEN);
makeError(M_BAD_JSON);
makeError(M_NOT_JSON);
makeError(M_LIMIT_EXCEEDED);
makeError(M_USER_IN_USE);
makeError(M_INVALID_USERNAME);
makeError(M_ROOM_IN_USE);
makeError(M_BAD_PAGINATION);
makeError(M_THREEPID_IN_USE);
makeError(M_THREEPID_NOT_FOUND);
makeError(M_SERVER_NOT_TRUSTED);
makeError(M_WEAK_PASSWORD);
makeError(M_UNKNOWN);
makeError(M_EXCLUSIVE);
makeError(M_MISSING_PARAM);
