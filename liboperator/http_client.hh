/** \brief client functionality using the HTTP-based API
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_HTTPCLIENT_HH_
#define __OPERATOR_HTTPCLIENT_HH_

#include "client.hh"
#include "structs.hh"
#include "http.hh"

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <istream>

namespace Operator {
  namespace Client {
    /** \example liboperator-glib/examples/versions.cc
     */
    /** \example liboperator-qt/examples/versions.cc
     */
    /** \example liboperator-glib/examples/sync.cc
     */
    /** \example liboperator-qt/examples/sync.cc
     */
    /** \example liboperator-glib/examples/chathistory.cc
     */
    /** \example liboperator-qt/examples/chathistory.cc
     */
    /** \example liboperator-glib/examples/send.cc
     */
    /** \example liboperator-qt/examples/send.cc
     */
    /** \brief low-level Client-Server API over HTTP.
     *
     * \sa [C-S r0.2.0](https://matrix.org/docs/spec/client_server/r0.2.0.html)
     */
    class HTTPClientAPI {
    public:
      HTTPClientAPI(HTTP::Adapter &, const std::string &server);

      /** \brief get the versions of the specification supported by the server.
       * \param[out] callback The callback will receive a vector of version strings.
       * \sa (C-S r0.2.0 section 2.1)
       */
      void versions(const callback_t<const std::vector<std::string> &> &callback);

      /** \brief log in using username and password
       * \param[in] username The username to log in as
       * \param[in] password The password to use
       * \param[out] callback The callback will receive the user's Matrix ID
       * and the server name
       * \sa (C-S r0.2.0 section 3.2.1)
       */
      void login(const std::string &username, const std::string &password, const callback_t<const std::string&,const std::string&> &callback);

      /** \brief log in using third party ID and pasword.
       * \sa (C-S r0.2.0 section 3.2.1)
       */
      void loginWith3PID(const std::string &medium, const std::string &address, const std::string &password, const callback_t<const std::string&,const std::string&> &);

      /** \brief log in with an access token that has previously been obtained.
       */
      void loginWithToken(const std::string &token);

      /** \brief register a new account.
       * \sa (C-S r0.2.0 section 3.3.1)
       */
      void registerAccount(const std::string &username, const std::string &password, const callback_t<std::string,std::string> &);

      /** \brief log out.
       * \sa (C-S r0.2.0 section 3.2.2)
       */
      void logout(const callback_t<> &);

      /** \brief determine if the client is currently logged in.
       */
      bool isLoggedIn() const {
        return !token.empty();
      }

      /** \brief return the access token for the client session.
       */
      const std::string &getToken() const {
        return token;
      }

      /** \brief get events from the server.
       * \sa (C-S r0.2.0 section 6.2.1)
       */
      void sync(const std::string &since, const std::string &filter, bool full_state, std::string set_presence, size_t timeout, const callback_t<const std::string &, nlohmann::json, nlohmann::json> &);

      /** \brief get messages/state events from a room.
       * \sa (C-S r0.2.0 section 6.3.5)
       */
      void roomMessages(const std::string &roomId, const std::string &from, const std::string &to, const std::string &dir, int limit, const callback_t<const std::string &, const std::string &, const std::vector<nlohmann::json> &> &);

      /** \brief send a message event to a room.
       * \sa (C-S r0.2.0 section 6.4.3)
       */
      void sendMessage(const std::string &roomId, const std::string &eventType, const std::string &txnId, const nlohmann::json &content, const callback_t<const std::string &> &);
    protected:
      HTTP::Adapter &adapter;
      std::string baseurl;
    private:
      mutable std::string token;

      void handleLogin(std::unique_ptr<Error::Error>, short code, const HTTP::headers_t &, std::unique_ptr<std::istream>, callback_t<const std::string&,const std::string&>) throw ();
      void handleLogout(std::unique_ptr<Error::Error>, short code, const HTTP::headers_t &, std::unique_ptr<std::istream>, callback_t<>) throw ();
    };

    /** \brief high-level Client-Server API over HTTP.
     */
    class HTTPClient : public Client {
    /** \example liboperator-glib/examples/echo.cc
     */
    /** \example liboperator-qt/examples/echo.cc
     */
    public:
      HTTPClient(HTTP::Adapter &adapter, const std::string &server);

      void login(const std::string &username, const std::string &password, const callback_t<const std::string &,const std::string &> &callback = noop_callback<const std::string &,const std::string &>);
      void loginWith3PID(const std::string &medium, const std::string &address, const std::string &password, const callback_t<const std::string &,const std::string &> &callback = noop_callback<const std::string &,const std::string &>);
      void loginWithToken(const std::string &token) {
        client.loginWithToken(token);
        trigger(Event::Login("", ""));
      }

      void logout(const callback_t<> &callback = noop_callback<>);

      bool isLoggedIn() const { return client.isLoggedIn(); }

      void startClient();
      void stopClient();

      void sendMessage(const Event::MessageEvent &, const callback_t<const std::string &> & = noop_callback<const std::string &>);
    protected:
      HTTPClientAPI client;
    private:
      void handleSync(std::shared_ptr<Error::Error>, const std::string &, nlohmann::json, nlohmann::json) throw ();
      callback_t<const std::string &, nlohmann::json, nlohmann::json> handleSync_ptr;
      std::function<void (const Event::Event &)> trigger_ptr;
      std::function<void (const std::string &, std::shared_ptr<Error::Error>, const std::string &)> triggerError_ptr;

      bool sync_pending;
      bool sync_active;
      std::string next_batch;
      unsigned int txn_counter;

    };
  }
}

#endif // __OPERATOR_HTTPCLIENT_HH_
