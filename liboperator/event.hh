/** \brief Matrix events
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_EVENT_HH_
#define __OPERATOR_EVENT_HH_

#include "error.hh"

#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <json.hpp>

namespace Operator {
  namespace Event {
    /** \brief base event class.
     */
    class Event {
    public:
      Event(nlohmann::json d, const std::string &_room_id = "") throw (Error::Error);
      virtual ~Event() {}

      const std::string &getType() const throw () { return data.at("type").get_ref<const std::string &>(); }
      const nlohmann::json &getContent() const throw () { return data.at("content"); }
      const nlohmann::json &getRawData() const throw () { return data; }
      const std::string &getRoomID() const throw () { return room_id; }

      typedef std::function<std::unique_ptr<Event>(nlohmann::json data, const std::string &)> event_builder_t;
      static std::unique_ptr<Event> newEvent(nlohmann::json data, const std::string & = "") throw (Error::Error);
      static void registerEventClass(const std::string &type, event_builder_t);

      template<typename T> static std::unique_ptr<Event> _new_helper(nlohmann::json data, const std::string &room_id) {
        return std::unique_ptr<Event>(new T(std::move(data), room_id));
      }

      static const std::string TYPE;
    protected:
      nlohmann::json data;
      std::string room_id;
      const std::string &get_room_name(const nlohmann::json &data, const std::string &room_id) {
        static std::string emptystring;
        if (!room_id.empty())
        {
          return room_id;
        }
        auto it = data.find("room_id");
        if (it == data.end() || !it->is_string())
        {
          return emptystring;
        }
        return it->get_ref<const std::string &>();
      }
    };

    /** \brief event that is associated with a room.
     */
    class RoomEvent : public Event {
    public:
      RoomEvent(nlohmann::json d, const std::string &room_id = "") throw (Error::Error);

      const std::string &getEventID() const throw () { return data.at("event_id").get_ref<const std::string &>(); }
      const std::string &getSender() const throw () { return data.at("sender").get_ref<const std::string &>(); }
      const nlohmann::json &getUnsigned() const throw (std::out_of_range) { return data.at("unsigned"); }
      // TODO: getOriginServerTs, getAge, ...

      static std::unique_ptr<Event> newEvent(nlohmann::json data, const std::string & = "") throw (Error::Error);
      static void registerEventClass(const std::string &type, event_builder_t);
    };

    /** \brief event relating to the state of a room.
     */
    class StateEvent : public RoomEvent {
    public:
      StateEvent(nlohmann::json d, const std::string &room_id = "") throw (Error::Error);

      const std::string &getStateKey() const throw() { return data.at("state_key").get_ref<const std::string &>(); }
      const nlohmann::json &getPrevContent() const throw (std::out_of_range) {return data.at("prev_content"); }

      static std::unique_ptr<Event> newEvent(nlohmann::json data, const std::string & = "") throw (Error::Error);
      static void registerEventClass(const std::string &type, event_builder_t);
    };

    /** \brief aliases for a room.
     */
    class RoomAliases : public RoomEvent {
    public:
      RoomAliases(nlohmann::json d, const std::string &room_id = "") throw (Error::Error);

      const std::vector<std::string> getAliases() const throw () { return aliases; }

      static const std::string TYPE;
    private:
      std::vector<std::string> aliases;
    };

    /** \brief general "message" events (a.k.a. timeline events)
     */
    class MessageEvent : public RoomEvent {
    public:
      MessageEvent(nlohmann::json d, const std::string &room_id = "") throw (Error::Error) : RoomEvent(std::move(d), room_id) {}
      MessageEvent(const MessageEvent &other) throw ()
        : RoomEvent(other) {}

      static std::unique_ptr<Event> newEvent(nlohmann::json data, const std::string & = "") throw (Error::Error);
      static void registerEventClass(const std::string &type, event_builder_t);
    };

    namespace Message {
      /** \brief base class for messages (events of type \c m.room.message)
       */
      class Message : public MessageEvent {
      public:
        Message(nlohmann::json d, const std::string &room_id = "") throw (Error::Error);
        Message(const Message &other) throw ()
          : MessageEvent(other) {}
        Message(const std::string &msgtype, const std::string &body, const std::string &room_id) throw ();

        const std::string &getBody() const throw () { return getContent().at("body").get_ref<const std::string &>(); }
        const std::string &getMessageType() const throw () { return getContent().at("msgtype").get_ref<const std::string &>(); }

        static const std::string TYPE;

        static void registerMessageClass(const std::string &type, event_builder_t);
        static void registerMessageClass(const std::string &type, event_builder_t, event_builder_t);
        static std::unique_ptr<Event> newEvent(nlohmann::json data, const std::string & = "") throw (Error::Error);
      };

      /** \brief HTML version of a message class
       */
      template <class T> class _HTMLMessage : public T {
      public:
        _HTMLMessage<T>(nlohmann::json d, const std::string &room_id = "") throw (Error::Error)
          : T(std::move(d), room_id) {}
        _HTMLMessage<T>(const _HTMLMessage<T> &other) throw ()
          : T(other) {}
        _HTMLMessage<T>(const std::string &body, const std::string &html, const std::string &room_id)
          : T({
              {"type", Message::Message::TYPE},
              {"room_id", room_id},
              {"content", {
                  {"msgtype", T::MSGTYPE},
                  {"body", body},
                  {"format", "org.matrix.custom.html"},
                  {"formatted_body", html}
                }}
            }) {}
      };

      /** \brief normal text messages (msgtype \c m.text)
       */
      class Text : public Message {
      public:
        Text(nlohmann::json d, const std::string &room_id = "") throw (Error::Error)
          : Message(std::move(d), room_id) {
          data["content"]["msgtype"] = MSGTYPE;
        }
        Text(const Text &other) throw ()
          : Message(other) {}
        Text(const std::string &body, const std::string &room)
          : Message(body, MSGTYPE, room) {}

        static const std::string MSGTYPE;
      };

      /** \brief normal HTML messages
       * \class Operator::Event::Message::HTML
       * \extends Text
       */
      using HTML = _HTMLMessage<Text>;

      /** \brief emote messages (msgtype \c m.emote)
       */
      class Emote : public Message {
      public:
        Emote(nlohmann::json d, const std::string &room_id = "") throw (Error::Error)
          : Message(std::move(d), room_id) {
          data["content"]["msgtype"] = MSGTYPE;
        }
        Emote(const Text &other) throw ()
          : Message(other) {}
        Emote(const std::string &body, const std::string &room)
          : Message(body, MSGTYPE, room) {}

        static const std::string MSGTYPE;
      };

      /** \brief HTML emote messages
       * \class Operator::Event::Message::HTMLEmote
       * \extends Emote
       */
      using HTMLEmote = _HTMLMessage<Emote>;

      /** \brief notice messages (msgtype \c m.notice)
       */
      class Notice : public Message {
      public:
        Notice(nlohmann::json d, const std::string &room_id = "") throw (Error::Error)
          : Message(std::move(d), room_id) {
          data["content"]["msgtype"] = MSGTYPE;
        }
        Notice(const Text &other) throw ()
          : Message(other) {}
        Notice(const std::string &body, const std::string &room)
          : Message(body, MSGTYPE, room) {}

        static const std::string MSGTYPE;
      };

      /** \brief HTML notice messages
       * \class Operator::Event::Message::HTMLNotice
       * \extends Notice
       */
      using HTMLNotice = _HTMLMessage<Notice>;

    }

    /** \brief the client has logged in
     */
    class Login : public Event {
    public:
      Login(const std::string &user_id, const std::string &home_server) throw (Error::Error)
        : Event({
            {"type", Login::TYPE},
            {"content", {
                {"user_id", user_id},
                {"home_server", home_server}
              }}
          }, "") {}

      const std::string &getUserID() const throw () { return getContent().at("user_id").get_ref<const std::string &>(); }
      const std::string &getHomeServer() const throw () { return getContent().at("home_server").get_ref<const std::string &>(); }

      static const std::string TYPE;
    };

    /** \brief the client has logged out
     */
    class Logout : public Event {
    public:
      Logout() throw (Error::Error)
        : Event({
            {"type", Logout::TYPE},
            {"content", nlohmann::json::object()}
          }, "") {}

      static const std::string TYPE;
    };

    /** \brief the user's membership in a room has changed
     */
    class RoomMembership : public Event {
    public:
      enum membership_t {NONE, INVITE, JOIN, KNOCK, LEAVE, BAN};
      RoomMembership(membership_t membership, const std::string &room_id) throw (Error::Error)
        : Event({
            {"type", RoomMembership::TYPE},
            {"room_id", room_id},
            {"content", {
                {"membership", membership}
              }}
          }, room_id) {}

      membership_t getMembership() const throw () { return static_cast<membership_t>(getContent().at("membership").get<int>()); }

      static const std::string TYPE;
    };

    /** \brief indicates that the sync result has been limited.
     *
     * This may indicate that there is a gap in the timeline
     */
    class Limited : public MessageEvent {
    public:
      Limited(const std::string &room_id) throw (Error::Error)
        : MessageEvent({
            {"type", Limited::TYPE},
            {"room_id", room_id},
            {"content", nlohmann::json::object()}
          }, room_id) {}

      static const std::string TYPE;
    };
  }
}

#endif  // __OPERATOR_EVENT_HH_
