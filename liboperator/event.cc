#include "event.hh"

#include <map>

using namespace Operator;
using namespace Operator::Event;

const std::string Operator::Event::Event::TYPE = "*";
const std::string RoomAliases::TYPE = "m.room.aliases";
const std::string Message::Message::TYPE = "m.room.message";
const std::string Message::Text::MSGTYPE = "m.text";
const std::string Message::Emote::MSGTYPE = "m.emote";
const std::string Message::Notice::MSGTYPE = "m.notice";

const std::string Login::TYPE = "ca.uhoreg.operator.__internal__.login";
const std::string Logout::TYPE = "ca.uhoreg.operator.__internal__.logout";
const std::string RoomMembership::TYPE = "ca.uhoreg.operator.__internal__.room_membership";
const std::string Limited::TYPE = "ca.uhoreg.operator.__internal__.limited";

namespace {
  typedef std::map<std::string,Operator::Event::Event::event_builder_t> event_builder_map_t;

#define eventPair(c) {c::TYPE, Operator::Event::Event::_new_helper<c>}
}

namespace {
  template<class T> inline std::unique_ptr<Operator::Event::Event> newEventHelper(nlohmann::json data, const std::string &room_id, const event_builder_map_t &builderMap) throw (Error::Error) {
    if (!data.is_object())
    {
      throw Error::InvalidData("Event data is not an object.");
    }
    auto type_it = data.find("type");
    if (type_it == data.end() || !type_it->is_string())
    {
      throw Error::InvalidData("Event data has invalid type");
    }
    auto builder_it = builderMap.find(*type_it);
    if (builder_it != builderMap.end())
    {
      return std::move(builder_it->second(std::move(data), room_id));
    }
    else
    {
      return std::unique_ptr<Operator::Event::Event>(new T(std::move(data), room_id));
    }
  }
}

Operator::Event::Event::Event(nlohmann::json d, const std::string &_room_id) throw (Error::Error)
  : data(std::move(d))
  , room_id(get_room_name(data, _room_id))
{
  if (!data.is_object())
  {
    throw Error::InvalidData("data is not a JSON object");
  }

  auto it = data.find("type");
  if (it == data.end())
  {
    throw Error::InvalidData("no type");
  }
  if (!it->is_string())
  {
    throw Error::InvalidData("type is not a string");
  }

  it = data.find("content");
  if (it == data.end())
  {
    throw Error::InvalidData("no content");
  }
  if (!it->is_object())
  {
    throw Error::InvalidData("content is not an object");
  }
}

namespace {
  event_builder_map_t eventBuilderMap = {
    eventPair(RoomAliases),
    {Message::Message::TYPE, Message::Message::newEvent}
  };
}

void Operator::Event::Event::registerEventClass(const std::string &type, event_builder_t builder) {
  eventBuilderMap[type] = builder;
}

std::unique_ptr<Operator::Event::Event> Operator::Event::Event::newEvent(nlohmann::json data, const std::string &room_id) throw (Error::Error) {
  return newEventHelper<Operator::Event::Event>(std::move(data), room_id, eventBuilderMap);
}

Operator::Event::RoomEvent::RoomEvent(nlohmann::json d, const std::string &_room_id) throw (Error::Error)
  : Event(std::move(d), _room_id) {
  if (room_id.empty())
  {
    throw Error::InvalidData("no room ID");
  }

  auto it = data.find("event_id");
  if (it != data.end() && !it->is_string())
  {
    throw Error::InvalidData("event ID is not a string");
  }

  it = data.find("sender");
  if (it != data.end() && !it->is_string())
  {
    throw Error::InvalidData("sender ID is not a string");
  }

  it = data.find("unsigned");
  if (it != data.end() && !it->is_object())
  {
    throw Error::InvalidData("unsigned is not an object");
  }
}

namespace {
  event_builder_map_t roomEventBuilderMap = {
    eventPair(RoomAliases),
    {Message::Message::TYPE, Message::Message::newEvent}
  };
}

void RoomEvent::registerEventClass(const std::string &type, event_builder_t builder) {
  roomEventBuilderMap[type] = builder;
}

std::unique_ptr<Operator::Event::Event> RoomEvent::newEvent(nlohmann::json data, const std::string &room_id) throw (Error::Error) {
  return newEventHelper<Operator::Event::RoomEvent>(std::move(data), room_id, roomEventBuilderMap);
}

Operator::Event::StateEvent::StateEvent(nlohmann::json d, const std::string &room_id) throw (Error::Error)
  : RoomEvent(std::move(d), room_id)
{
  auto it = data.find("state_key");
  if (it == data.end())
  {
    throw Error::InvalidData("no state key");
  }
  if (!it->is_string())
  {
    throw Error::InvalidData("state key is not a string");
  }

  it = data.find("prev_content");
  if (it != data.end() && !it->is_object())
  {
    throw Error::InvalidData("previous content is not an object");
  }
}

namespace {
  event_builder_map_t stateEventBuilderMap = {
    eventPair(RoomAliases)
  };
}

void StateEvent::registerEventClass(const std::string &type, event_builder_t builder) {
  stateEventBuilderMap[type] = builder;
}

std::unique_ptr<Operator::Event::Event> StateEvent::newEvent(nlohmann::json data, const std::string &room_id) throw (Error::Error) {
  return newEventHelper<Operator::Event::StateEvent>(std::move(data), room_id, stateEventBuilderMap);
}

Operator::Event::RoomAliases::RoomAliases(nlohmann::json d, const std::string &room_id) throw (Error::Error)
  : RoomEvent(std::move(d), room_id)
{
  auto it = data.find("aliases");
  if (it == data.end())
  {
    throw Error::InvalidData("no aliases");
  }
  if (!it->is_array())
  {
    throw Error::InvalidData("aliases is not an array");
  }
  for (auto aliases_it = it->begin(); aliases_it != it->end(); ++aliases_it)
  {
    if (!aliases_it->is_string())
    {
      throw Error::InvalidData("alias is not a string");
    }
    aliases.push_back(aliases_it->get<std::string>());
  }
}

namespace {
  event_builder_map_t messageEventBuilderMap = {
    {Message::Message::TYPE, Message::Message::newEvent}
  };
}

void MessageEvent::registerEventClass(const std::string &type, event_builder_t builder) {
  messageEventBuilderMap[type] = builder;
}

std::unique_ptr<Operator::Event::Event> MessageEvent::newEvent(nlohmann::json data, const std::string &room_id) throw (Error::Error) {
  return newEventHelper<Operator::Event::MessageEvent>(std::move(data), room_id, messageEventBuilderMap);
}

Operator::Event::Message::Message::Message(nlohmann::json d, const std::string &room_id) throw (Error::Error)
  : MessageEvent(std::move(d), room_id) {
  auto content = getContent();
  auto it = content.find("body");
  if (it == content.end())
  {
    throw Error::InvalidData("no body");
  }
  if (!it->is_string())
  {
    throw Error::InvalidData("body is not a string");
  }

  it = content.find("msgtype");
  if (it == content.end())
  {
    throw Error::InvalidData("no message type");
  }
  if (!it->is_string())
  {
    throw Error::InvalidData("message type is not a string");
  }
}

Operator::Event::Message::Message::Message(const std::string &msgtype, const std::string &body, const std::string &room_id) throw ()
  : MessageEvent({
        {"type", Message::Message::TYPE},
        {"room_id", room_id},
        {"content", {
            {"msgtype", msgtype},
            {"body", body}
          }}
      })
{}

namespace {
#define messagePair(c, c2) {Message::c::MSGTYPE, {Operator::Event::Event::_new_helper<Message::c>, Operator::Event::Event::_new_helper<Message::c2>}}
  std::map<std::string,std::pair<Operator::Event::Event::event_builder_t,Operator::Event::Event::event_builder_t> > messageBuilderMap = {
    messagePair(Text, HTML),
    messagePair(Emote, HTMLEmote),
    messagePair(Notice, HTMLNotice)
  };
}

void Message::Message::registerMessageClass(const std::string &type, event_builder_t builder) {
  messageBuilderMap[type] = {builder, builder};
}

void Message::Message::registerMessageClass(const std::string &type, event_builder_t builder1, event_builder_t builder2) {
  messageBuilderMap[type] = {builder1, builder2};
}

std::unique_ptr<Operator::Event::Event> Message::Message::newEvent(nlohmann::json data, const std::string &room_id) throw (Error::Error) {
  if (!data.is_object())
  {
    throw Error::InvalidData("Event data is not an object.");
  }
  auto content_it = data.find("content");
  if (content_it == data.end() || !content_it->is_object())
  {
    throw Error::InvalidData("Event has invalid content");
  }
  auto type_it = content_it->find("msgtype");
  if (type_it == content_it->end() || !type_it->is_string())
  {
    throw Error::InvalidData("Event data has invalid message type");
  }
  auto builder_it = messageBuilderMap.find(*type_it);
  if (builder_it != messageBuilderMap.end())
  {
    auto format_it = content_it->find("format");
    auto formatted_body = content_it->find("formatted_body");
    if (format_it != content_it->end() && format_it->is_string()
        && (*format_it) == "org.matrix.custom.html"
        && formatted_body != content_it->end() && formatted_body->is_string())
    {
      return std::move(builder_it->second.second(std::move(data), room_id));
    }
    else
    {
      return std::move(builder_it->second.first(std::move(data), room_id));
    }
  }
  else
  {
    return std::unique_ptr<Operator::Event::Event>(new Message(std::move(data), room_id));
  }
}
