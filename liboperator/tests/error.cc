/** \brief test cases for error classes
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../error.hh"

#define BOOST_TEST_MODULE Error
#include <boost/test/included/unit_test.hpp>

using namespace Operator::Error;

BOOST_AUTO_TEST_CASE(Matrix_Error)
{
  BOOST_CHECK(dynamic_cast<MatrixForbidden*>(MatrixError::newError(403, "{\"errcode\": \"M_FORBIDDEN\", \"error\": \"Permission denied\"}"_json)));
}
