/** \brief test cases for event classes
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../event.hh"

#define BOOST_TEST_MODULE Error
#include <boost/test/included/unit_test.hpp>

#include <iostream>

using namespace Operator::Event;

BOOST_AUTO_TEST_CASE(Message_text_from_Event)
{
  std::unique_ptr<Event> event = Event::newEvent("{\"type\": \"m.room.message\", \"event_id\": \"$123:example.com\", \"sender\": \"@me:example.com\", \"content\": {\"body\": \"Hello World!\", \"msgtype\": \"m.text\"}}"_json, "!abc:example.com");
  BOOST_CHECK(dynamic_cast<Message::Message*>(event.get()));
  BOOST_CHECK(dynamic_cast<Message::Text*>(event.get()));
  BOOST_CHECK(!dynamic_cast<Message::HTML*>(event.get()));
}

BOOST_AUTO_TEST_CASE(Message_notice_from_Event)
{
  std::unique_ptr<Event> event = Event::newEvent("{\"type\": \"m.room.message\", \"event_id\": \"$123:example.com\", \"sender\": \"@me:example.com\", \"content\": {\"body\": \"Hello World!\", \"msgtype\": \"m.notice\"}}"_json, "!abc:example.com");
  BOOST_CHECK(dynamic_cast<Message::Message*>(event.get()));
  BOOST_CHECK(dynamic_cast<Message::Notice*>(event.get()));
}

BOOST_AUTO_TEST_CASE(Message_notice_from_MessageEvent)
{
  std::unique_ptr<Event> event = MessageEvent::newEvent("{\"type\": \"m.room.message\", \"event_id\": \"$123:example.com\", \"sender\": \"@me:example.com\", \"content\": {\"body\": \"Hello World!\", \"msgtype\": \"m.notice\"}}"_json, "!abc:example.com");
  BOOST_CHECK(dynamic_cast<Message::Message*>(event.get()));
  BOOST_CHECK(dynamic_cast<Message::Notice*>(event.get()));
}

BOOST_AUTO_TEST_CASE(create_HTML_message)
{
  auto message = Message::HTML("Hello World!", "<p>Hello World!</p>", "!abc:example.com");
  BOOST_CHECK(message.getContent() == "{\"body\": \"Hello World!\", \"msgtype\": \"m.text\", \"format\": \"org.matrix.custom.html\", \"formatted_body\": \"<p>Hello World!</p>\"}"_json);
}

BOOST_AUTO_TEST_CASE(parse_HTML_message)
{
  std::unique_ptr<Event> event = Message::Message::newEvent("{\"type\": \"m.room.message\", \"event_id\": \"$123:example.com\", \"sender\": \"@me:example.com\", \"content\": {\"body\": \"Hello World!\", \"msgtype\": \"m.text\", \"format\": \"org.matrix.custom.html\", \"formatted_body\": \"<p>Hello World!</p>\"}}"_json, "!abc:example.com");
  BOOST_CHECK(dynamic_cast<Message::Message*>(event.get()));
  BOOST_CHECK(dynamic_cast<Message::Text*>(event.get()));
  BOOST_CHECK(dynamic_cast<Message::HTML*>(event.get()));
}

BOOST_AUTO_TEST_CASE(Room_Membership)
{
  auto event = RoomMembership(RoomMembership::JOIN, "!abc:example.com");
  BOOST_CHECK(event.getMembership() == RoomMembership::JOIN);
}

BOOST_AUTO_TEST_CASE(Event_Limited)
{
  auto event = Limited("!abc:example.com");
  BOOST_CHECK(event.getType() == Limited::TYPE);
}
