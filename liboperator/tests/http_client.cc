/** \brief test cases for HTTP client API
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../http_client.hh"

#include <memory>
#include <iostream>
#include <sstream>
#include <vector>

#define BOOST_TEST_MODULE HTTP client
#include <boost/test/included/unit_test.hpp>

using namespace Operator;

class TestAdapter : public Operator::HTTP::Adapter {
public:
  typedef struct {
    struct {
      std::string method;
      std::string URL;
      Operator::HTTP::headers_t headers;
      std::string body;
    } request;
    struct {
      short code;
      Operator::HTTP::headers_t headers;
      std::string body;
    } response;
  } response_t;
  std::vector<response_t> responses;
  int count = 0;
  TestAdapter(std::vector<response_t> _responses)
    : responses(_responses)
    {}
  void httpcall(const std::string &method, const std::string &URL, const Operator::HTTP::headers_t &headers, std::unique_ptr<std::istream> body, size_t timeout, const Operator::HTTP::httpcallback_t &callback) {
    response_t &response = responses[count++];
    BOOST_CHECK(method == response.request.method);
    BOOST_CHECK(URL == response.request.URL);
    // FIXME: check headers
    if (body == nullptr) {
      BOOST_CHECK(response.request.body == "");
    }
    else
    {
      std::string b;
      (*body) >> b;
      BOOST_CHECK(b == response.request.body);
    }
    std::istringstream *res_body = new std::istringstream(response.response.body);
    callback(nullptr, response.response.code, response.response.headers, std::unique_ptr<std::istream>(res_body));
  }
};

Operator::HTTP::headers_t emptyheaders;

BOOST_AUTO_TEST_CASE(versions)
{
  TestAdapter adapter({
      {
        {"GET", "https://test.nowhere/_matrix/client/versions", emptyheaders, ""},
        {200, emptyheaders, "{\"versions\": [\"r0.0.1\",\"r0.1.0\",\"r0.2.0\"]}"}
      }
    });
  Operator::Client::HTTPClientAPI client(adapter, "https://test.nowhere/");
  std::vector<std::string> expected = {"r0.0.1", "r0.1.0", "r0.2.0"};
  std::vector<std::string> actual;
  client.versions([&actual](std::shared_ptr<Error::Error> error, const std::vector<std::string> &versions) {
      BOOST_REQUIRE(!error);
      actual = versions;
    });

  BOOST_REQUIRE(actual.size() == expected.size());
  for (unsigned int i = 0; i < expected.size(); ++i)
  {
    BOOST_REQUIRE(actual[i] == expected[i]);
  }
}

BOOST_AUTO_TEST_CASE(login_logout)
{
  TestAdapter adapter({
      {
        {"POST", "https://test.nowhere/_matrix/client/r0/login", emptyheaders, "{\"password\":\"sekrit\",\"type\":\"m.login.password\",\"user\":\"user\"}"},
        {200, emptyheaders, "{\"user_id\": \"@user:test.nowhere\", \"access_token\": \"abc123\", \"home_server\": \"test.nowhere\"}"}
      },
      {
        {"POST", "https://test.nowhere/_matrix/client/r0/logout?access_token=abc123", emptyheaders, ""},
        {200, emptyheaders, "{}"}
      }
    });
  Operator::Client::HTTPClientAPI client(adapter, "https://test.nowhere/");
  client.login("user", "sekrit", [&client](std::shared_ptr<Error::Error> error, const std::string &user_id, const std::string &home_server) {
      BOOST_REQUIRE(!error);
      BOOST_CHECK(user_id == "@user:test.nowhere");
      BOOST_CHECK(home_server == "test.nowhere");
      BOOST_CHECK(client.isLoggedIn());
      BOOST_CHECK(client.getToken() == "abc123");
      client.logout([&client](std::shared_ptr<Error::Error> error) {
          BOOST_REQUIRE(error == nullptr);
          BOOST_CHECK(!client.isLoggedIn());
          BOOST_CHECK(client.getToken().empty());
        });
    });
}

BOOST_AUTO_TEST_CASE(login_with_3pid)
{
  TestAdapter adapter ({
      {
        {"POST", "https://test.nowhere/_matrix/client/r0/login", emptyheaders, "{\"address\":\"user@example.com\",\"medium\":\"email\",\"password\":\"sekrit\",\"type\":\"m.login.password\"}"},
        {200, emptyheaders, "{\"user_id\": \"@user:test.nowhere\", \"access_token\": \"abc123\", \"home_server\": \"test.nowhere\"}"}
      }
    });
  Operator::Client::HTTPClientAPI client(adapter, "https://test.nowhere/");
  client.loginWith3PID("email", "user@example.com", "sekrit", [&client](std::shared_ptr<Error::Error> error, const std::string &user_id, const std::string &home_server) {
      BOOST_REQUIRE(!error);
      BOOST_CHECK(user_id == "@user:test.nowhere");
      BOOST_CHECK(home_server == "test.nowhere");
      BOOST_CHECK(client.isLoggedIn());
      BOOST_CHECK(client.getToken() == "abc123");
    });
}

BOOST_AUTO_TEST_CASE(login_failure)
{
  TestAdapter adapter({
      {
        {"POST", "https://test.nowhere/_matrix/client/r0/login", emptyheaders, "{\"password\":\"sekrit\",\"type\":\"m.login.password\",\"user\":\"user\"}"},
        {203, emptyheaders, "{\"errcode\": \"M_FORBIDDEN\", \"error\": \"something something not allowed\"}"}
      }
    });
  Operator::Client::HTTPClientAPI client(adapter, "https://test.nowhere/");
  client.login("user", "sekrit", [&client](std::shared_ptr<Error::Error> error, const std::string &user_id, const std::string &home_server) {
      BOOST_REQUIRE(error);
      BOOST_REQUIRE(dynamic_cast<Error::MatrixForbidden*>(error.get()));
    });
}
