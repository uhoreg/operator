/*! Qt support for HTTP-based protocols
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __OPERATOR_QT_HTTP_HH_
#define __OPERATOR_QT_HTTP_HH_

#include <QtNetwork>

#include <liboperator/http_client.hh>
#include <liboperator/error.hh>

#include <memory>

namespace Operator {
  /*! Qt support
   */
  namespace Qt {
    class HTTPAdapter : public HTTP::Adapter {
    public:
      HTTPAdapter(QNetworkAccessManager *);
      void httpcall(const std::string &method, const std::string &URL, const HTTP::headers_t &, std::unique_ptr<std::istream> body, size_t timeout, const HTTP::httpcallback_t &);
    private:
      QNetworkAccessManager *manager;
    };

    class NetworkError : public Error::Error {
    public:
      NetworkError(QNetworkReply::NetworkError _err, std::string _msg)
        : err(_err),
          msg(_msg)
        {}
      std::string to_string() const { return msg; };
      QNetworkReply::NetworkError getQtError() const { return err; }
    private:
      QNetworkReply::NetworkError err;
      std::string msg;
    };
  }
}

#endif // __OPERATOR_QT_HTTP_HH_
