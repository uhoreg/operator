#include "../http.hh"

#include <QCoreApplication>

#include <iostream>
#include <string>
#include <cstring>

using namespace Operator;

int main(int argc, char**argv)
{
  if (argc != 2 || !strcmp(argv[1], "--help"))
  {
    std::cout << "Usage:\n";
    std::cout << "  " << argv[0] << " <homeserver>\n";
    exit(0);
  }

  QCoreApplication app(argc, argv);
  QNetworkAccessManager *manager = new QNetworkAccessManager();

  Operator::Qt::HTTPAdapter adapter(manager);
  Operator::Client::HTTPClientAPI client(adapter, argv[1]);
  client.versions([&app](std::shared_ptr<Error::Error> error, const std::vector<std::string> &versions) {
      if (error)
      {
        std::cerr << "Error getting version: " << error->to_string() << std::endl;
      }
      else
      {
        for (std::vector<std::string>::const_iterator it = versions.begin(); it != versions.end(); ++it)
        {
          std::cout << (*it) << std::endl;
        }
      }
      app.exit();
    });
  return app.exec();
}
