Operator-Qt                                                       {#Operator-Qt}
===========

Operator-Qt provides an adapter for using Operator with the Qt library.  See
the Operator::Qt namespace documentation for more information.
