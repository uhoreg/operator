/*! Qt support for HTTP-based protocols
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "http.hh"

#include <memory>
#include <sstream>

using namespace Operator;
using namespace Operator::Qt;

class Handler : public QObject {
  Q_OBJECT

  HTTP::httpcallback_t callback;
  QNetworkReply *reply;
  QBuffer *buffer;

public:
  Handler(HTTP::httpcallback_t c, QNetworkReply *r, QBuffer *b)
    : callback(c)
    , reply(r)
    , buffer(b)
    {}

  ~Handler() {
    if (buffer)
    {
      delete buffer;
    }
  }

public slots:
  void finished() {
    QByteArray bodyArray = reply->readAll();
    std::istringstream *res_body = new std::istringstream(std::string(bodyArray.data(), bodyArray.length()));
    HTTP::headers_t resp_headers;
    const QList<QNetworkReply::RawHeaderPair> &headers = reply->rawHeaderPairs();
    for (QList<QNetworkReply::RawHeaderPair>::const_iterator it = headers.begin(); it != headers.end(); ++it)
    {
      const QByteArray &name = it->first;
      const QByteArray &value = it->second;
      resp_headers.push_back({std::string(name.data(), name.length()), std::string(value.data(), value.length())});
    }
    callback(nullptr, reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), resp_headers, std::unique_ptr<std::istream>(res_body));
    reply->deleteLater();
    this->deleteLater();
  }

  void error(QNetworkReply::NetworkError e) {
    callback(std::unique_ptr<Operator::Error::Error>(new NetworkError(e, reply->errorString().toStdString())), 0, {}, nullptr);
    reply->deleteLater();
    this->deleteLater();
  }
};
