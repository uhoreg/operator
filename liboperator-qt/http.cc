/*! Qt support for HTTP-based protocols
 * \file
 * \author Hubert Chathi <hubert@uhoreg.ca>
 * \copyright 2017 Hubert Chathi
 * \copyright
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * \copyright
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "http.hh"
#include "private.hh"

#include <iostream>
#include <sstream>

using namespace Operator;
using namespace Operator::Qt;

HTTPAdapter::HTTPAdapter(QNetworkAccessManager *m)
  : manager(m) {}

void HTTPAdapter::httpcall(const std::string &method, const std::string &URL, const HTTP::headers_t &headers, std::unique_ptr<std::istream> body, size_t timeout, const HTTP::httpcallback_t &callback) {
  QNetworkRequest request;
  request.setUrl(QUrl::fromEncoded(URL.c_str(), QUrl::StrictMode));
  for (HTTP::headers_t::const_iterator it = headers.begin(); it != headers.end(); ++it)
  {
    request.setRawHeader(it->first.c_str(), it->second.c_str());
  }
  QBuffer *body_buffer = nullptr;
  if (body)
  {
    char buffer[1024];
    body_buffer = new QBuffer();
    body_buffer->open(QIODevice::WriteOnly);
    do
    {
      body->read(buffer, 1024);
      if (body->gcount())
      {
        body_buffer->write(buffer, body->gcount());
      }
    }
    while (body && body->gcount());
    body_buffer->close();
  }
  QNetworkReply *reply = manager->sendCustomRequest(request, method.c_str(), body_buffer);
  Handler *handler = new Handler(callback, reply, body_buffer);
  QObject::connect(reply, SIGNAL(finished()), handler, SLOT(finished()));
  QObject::connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), handler, SLOT(error(QNetworkReply::NetworkError)));
}
