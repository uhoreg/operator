Operator                                                             {#mainpage}
========

Operator is a C++ library for plugging into the [Matrix](https://matrix.org/).
Operator is runloop-agnostic: it does not make any network calls itself.
Rather, the calling program passes in an adapter that defines how to perform
any network calls.  Operator currently includes adapters for GLib and Qt, and
more will be written in the future.

Operator currently includes a low-level client API and a high-level client API;
a low-level AS API, and high-level AS API are planned.

Requirements
------------
The main Operator library depends on:

- [nlohmann's json library](https://github.com/nlohmann/json)
- [Boost](http://www.boost.org/) (for testing)

In addition, the adapters require other libraries:

- [GLib adapter](@ref Operator-GLib)
  - [GLib](https://www.gtk.org/) 2
  - [libsoup](https://wiki.gnome.org/Projects/libsoup) 2.4
- [Qt adapter](@ref Operator-Qt)
  - [Qt](http://qt-project.org/) 5

Copyright and License
---------------------

Copyright 2017 Hubert Chathi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
